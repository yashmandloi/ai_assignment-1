using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyChaseState : BlinkyBaseState
{
	private Animator _animator;

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Into chase state!");
		_animator = animator;
		GameDirector.Instance.GameStateChanged.AddListener(BlinkyStateChanged);
	}

	public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		//if(GameDirector.States.enState_Normal)
        ghostController.SetMoveToLocation(ghostController.PacMan.position);
	}

	public void BlinkyStateChanged(GameDirector.States _state)
	{
		switch (_state)
		{
			case GameDirector.States.enState_Normal:
				_animator.SetBool("IsRunningAway", false);
				break;

			case GameDirector.States.enState_PacmanInvincible:
				_animator.SetBool("IsRunningAway", true);
				break;

			case GameDirector.States.enState_GameOver:
				break;
		}
	}
}