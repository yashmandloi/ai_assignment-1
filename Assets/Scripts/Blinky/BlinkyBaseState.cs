using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyBaseState : FSMBaseState
{
    protected GhostController ghostController;

    public override void Init(GameObject _owner, FSM _fsm)
    {
        base.Init(_owner, _fsm);
        ghostController = owner.GetComponent<GhostController>();
        Debug.Assert(ghostController != null, $"{owner.name} - {name} must have a GhostController Component to run");
    }
}