using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyRunAwayState : BlinkyBaseState
{
	private Animator _animator;

	private float runAwayDistanceThreshold = 3f;
	private bool isAtRunAwayPosition;
	private int maxX = 8, minX = -8, maxY = 9, minY = -10, origin = 0;
	private int temp1, temp2;

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Into runaway state!");
		_animator = animator;
		GameDirector.Instance.GameStateChanged.AddListener(BlinkyStateChanged);
		isAtRunAwayPosition = true;
	}

	public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		RunAway();
		ghostController.killedEvent.AddListener(Killed);
	}

	public void BlinkyStateChanged(GameDirector.States _state)
	{
		switch (_state)
		{
			case GameDirector.States.enState_Normal:
				_animator.SetBool("IsRunningAway", false);
				break;

			case GameDirector.States.enState_PacmanInvincible:
				_animator.SetBool("IsRunningAway", true);
				break;

			case GameDirector.States.enState_GameOver:
				break;
		}
	}

	void RunAway()
	{
		//pacman and ghost in same quadrant

		if (isAtRunAwayPosition)
		{
			//quad-1
			if (ghostController.transform.position.x > origin && ghostController.transform.position.y > origin && ghostController.PacMan.position.x > origin && ghostController.PacMan.position.y > origin)
			{
				//Debug.Log("3");
				temp1 = Random.Range(minX, origin + 1);
				temp2 = Random.Range(minY, origin + 1);
				isAtRunAwayPosition = false;
			}

			//quad-2
			else if (ghostController.transform.position.x < origin && ghostController.transform.position.y > origin && ghostController.PacMan.position.x < origin && ghostController.PacMan.position.y > origin)
			{
				//Debug.Log("1");
				temp1 = Random.Range(origin, maxX + 1);
				temp2 = Random.Range(origin, maxY + 1);
				isAtRunAwayPosition = false;
			}

			//quad-3
			else if (ghostController.transform.position.x < origin && ghostController.transform.position.y < origin && ghostController.PacMan.position.x < origin && ghostController.PacMan.position.y < origin)
			{
				//Debug.Log("4");
				temp1 = Random.Range(origin, maxX + 1);
				temp2 = Random.Range(minY, origin + 1);
				isAtRunAwayPosition = false;
			}

			//quad-4
			else if (ghostController.transform.position.x > origin && ghostController.transform.position.y < origin && ghostController.PacMan.position.x > origin && ghostController.PacMan.position.y < origin)
			{
				//Debug.Log("2");
				temp1 = Random.Range(minX, maxX + 1);
				temp2 = Random.Range(minY, maxY + 1);
				isAtRunAwayPosition = false;
			}
			else
			{
				temp1 = Random.Range(minX, origin + 1);
				temp2 = Random.Range(origin, maxY + 1);
				isAtRunAwayPosition = false;
			}
		}

		ghostController.SetMoveToLocation(new Vector2(temp1, temp2));

		if (ghostController.transform.position.x == temp1 && ghostController.transform.position.y == temp2)
		{
			isAtRunAwayPosition = true;
		}
	}

	void Killed()
    {
        _animator.SetBool("IsDead", true);
    }
}