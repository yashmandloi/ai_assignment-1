using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkyRespawnState : ClydeBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Into respawn state!");
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ghostController._animator.SetBool("IsDead", false);
        animator.SetBool("IsRunningAway", false);
        animator.SetBool("IsDead", false);
        animator.SetBool("IsReturningToSpawnPoint", false);
        animator.SetBool("IsRespawning", false);
        animator.SetBool("IsChasing", true);
    }
}