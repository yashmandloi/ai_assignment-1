using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkyReturnToSpawnPointState : ClydeBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Into return to spawn point state!");

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ghostController.SetMoveToLocation(ghostController.ReturnLocation);
        if(ghostController.transform.position.x == ghostController.ReturnLocation.x && ghostController.transform.position.y == ghostController.ReturnLocation.y)
            animator.SetTrigger("IsRespawning");
    }
}