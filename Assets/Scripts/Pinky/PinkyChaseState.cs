using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyChaseState : PinkyBaseState
{
	private Animator _animator;

	private int randomTimer1, randomTimer2, counter, counter1, counter2, temp1, temp2;
	private bool isAtChasePosition;
	private int maxX = 8, minX = -8, maxY = 9, minY = -10, origin = 0;
	private Vector2 prevPos;

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Into chase state!");
		_animator = animator;
		GameDirector.Instance.GameStateChanged.AddListener(PinkyStateChanged);
		counter = 0;
		counter1 = 0;
		counter2 = 0;
		isAtChasePosition = true;
	}

	public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		ChaseAlgo();
	}

	public void PinkyStateChanged(GameDirector.States _state)
	{
		switch (_state)
		{
			case GameDirector.States.enState_Normal:
				_animator.SetBool("IsRunningAway", false);
				break;

			case GameDirector.States.enState_PacmanInvincible:
				_animator.SetBool("IsRunningAway", true);
				break;

			case GameDirector.States.enState_GameOver:
				break;
		}
	}

	void ChaseAlgo()
	{
		if (isAtChasePosition)
		{
			//quad-1
			if (ghostController.transform.position.x > origin && ghostController.transform.position.y > origin)
			{
				temp1 = Random.Range(minX, maxX + 1);
				if (temp1 > origin)
				{
					//Debug.Log("4");
					temp2 = Random.Range(minY, origin + 1);
				}
				else
				{
					//Debug.Log("2 or 3");
					temp2 = Random.Range(minY, maxY + 1);
				}
				isAtChasePosition = false;
			}

			//quad-2
			else if (ghostController.transform.position.x < origin && ghostController.transform.position.y > origin)
			{
				temp1 = Random.Range(minX, maxX + 1);
				if (temp1 < origin)
				{
					//Debug.Log("3");
					temp2 = Random.Range(minY, origin + 1);
				}
				else
				{
					//Debug.Log("1 or 4");
					temp2 = Random.Range(minY, maxY + 1);
				}
				isAtChasePosition = false;
			}

			//quad-3
			else if (ghostController.transform.position.x < origin && ghostController.transform.position.y < origin)
			{
				temp1 = Random.Range(minX, maxX + 1);
				if (temp1 < origin)
				{
					//Debug.Log("2");
					temp2 = Random.Range(origin, maxY + 1);
				}
				else
				{
					//Debug.Log("1 or 4");
					temp2 = Random.Range(minY, maxY + 1);
				}
				isAtChasePosition = false;
			}

			//quad-4
			else if (ghostController.transform.position.x > origin && ghostController.transform.position.y < origin)
			{
				temp1 = Random.Range(minX, maxX + 1);
				if (temp1 > origin)
				{
					//Debug.Log("1");
					temp2 = Random.Range(origin, maxY + 1);
				}
				else
				{
					//Debug.Log("2 or 3");
					temp2 = Random.Range(minY, maxY + 1);
				}
				isAtChasePosition = false;
			}
			else
			{
				temp1 = Random.Range(minX, origin + 1);
				temp2 = Random.Range(origin, maxY + 1);
				isAtChasePosition = false;
			}
		}

		ghostController.SetMoveToLocation(new Vector2(temp1, temp2));

		if (ghostController.transform.position.x == temp1 && ghostController.transform.position.y == temp2)
		{
			counter1++;
			if (counter1 > 500)
			{
				isAtChasePosition = true;
				counter1 = 0;
			}
		}
	}
}