using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyDeathState : PinkyBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Into death state!");
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetTrigger("IsReturningToSpawnPoint");
    }
}