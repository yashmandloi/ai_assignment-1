using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyRunAwayState : PinkyBaseState
{
	private Animator _animator;

	private float runAwayDistanceThreshold = 3f;
	private bool isAtRunAwayPosition = false;
	private int maxX = 8, minX = -8, maxY = 9, minY = -10;

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		Debug.Log("Into runaway state!");
		_animator = animator;
		GameDirector.Instance.GameStateChanged.AddListener(PinkyStateChanged);
	}

	public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		RunAway();
		ghostController.killedEvent.AddListener(Killed);
	}

	public void PinkyStateChanged(GameDirector.States _state)
	{
		switch (_state)
		{
			case GameDirector.States.enState_Normal:
				_animator.SetBool("IsRunningAway", false);
				break;

			case GameDirector.States.enState_PacmanInvincible:
				_animator.SetBool("IsRunningAway", true);
				break;

			case GameDirector.States.enState_GameOver:
				break;
		}
	}

	void RunAway()
	{
		int temp1 = Random.Range(minX, maxX + 1), temp2 = Random.Range(minY, maxY + 1);
		if (Vector2.Distance(ghostController.transform.position, ghostController.PacMan.position) < runAwayDistanceThreshold || isAtRunAwayPosition)
		{
			Debug.Log(temp1 + " " + temp2);
			ghostController.SetMoveToLocation(new Vector2(temp1, temp2));
			isAtRunAwayPosition = false;
		}
		if (ghostController.transform.position.x == temp1 && ghostController.transform.position.y == temp2)
		{
			isAtRunAwayPosition = true;
		}
	}

	void Killed()
	{
		_animator.SetBool("IsDead", true);
	}
}