using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClydeChaseState : ClydeBaseState
{
	private Animator _animator;

	private int randomTimer1, randomTimer2, counter1, counter2, temp1, temp2;
	private bool isAtChasePosition;
	private int maxX = 8, minX = -8, maxY = 9, minY = -10, origin = 0;
	private Vector2 prevPos;

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Into chase state!");
		_animator = animator;
		GameDirector.Instance.GameStateChanged.AddListener(ClydeStateChanged);
		isAtChasePosition = true;
	}

	public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		ChaseAlgo();
	}

	public void ClydeStateChanged(GameDirector.States _state)
	{
		switch (_state)
		{
			case GameDirector.States.enState_Normal:
				_animator.SetBool("IsRunningAway", false);
				break;

			case GameDirector.States.enState_PacmanInvincible:
				_animator.SetBool("IsRunningAway", true);
				break;

			case GameDirector.States.enState_GameOver:
				break;
		}
	}

	void ChaseAlgo()
	{
		prevPos = ghostController.transform.position;
		if (isAtChasePosition)
		{
			temp1 = Random.Range(minX, maxX + 1);
			temp2 = Random.Range(minY, maxY + 1);

			isAtChasePosition = false;
		}
		ghostController.SetMoveToLocation(new Vector2(temp1, temp2));

		if (ghostController.transform.position.x == temp1 && ghostController.transform.position.y == temp2)
		{
			isAtChasePosition = true;
		}
        else if (ghostController.transform.position.x == prevPos.x && ghostController.transform.position.y == prevPos.y)
        {
			counter1++;
            if (counter1 > 500)
            {
				isAtChasePosition = true;
				counter1 = 0;
            }
        }
	}
}